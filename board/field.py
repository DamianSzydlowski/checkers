from tkinter import Button, StringVar

from Pieces.pawn import Pawn
from Pieces.piece import Piece
from Pieces.quenn import Queen
from handlers import exception
from handlers.SelectHandler import SelectHandler
from handlers.moveHandler import MoveH
from handlers.turnHandler import TurnHandler


class Field(Button):
    def __init__(self, master, x, y, piece=None, *args, **kwargs):
        Button.__init__(self, master, *args, **kwargs)
        self._piece = 0
        self._cordX = x
        self._cordY = y
        self._piece = piece
        self.config(command=lambda: self.clicked())
        self.TextToDisplay = StringVar()

        if piece != None:
            self.TextToDisplay = piece.getTextToDisplay()
        self.config(textvariable=self.TextToDisplay)

    @exception.TryBlock
    def clicked(self):
        if self._piece != None:
            if isinstance(self._piece, Piece):
                self._piece.selected()
        elif Pawn.countW == 0 or Pawn.countB == 0:
            raise exception.GameAlreadyEnded(self.master)
        else:
            if (TurnHandler.getPlayerTurn().getColor() == "B" and not MoveH.possibleAttackW) or (
                    TurnHandler.getPlayerTurn().getColor() == "C" and not MoveH.possibleAttackB):
                if SelectHandler.selected:
                    poss = SelectHandler.getSelCords()
                    if not MoveH.isQueen(poss[0], poss[1]):
                        if (poss[0] == self._cordX + 1 or poss[0] == self._cordX - 1) and poss[
                            1] == self._cordY + 1 and TurnHandler.getPlayerTurn().getColor() == "B":
                            MoveH.Move(self._cordX, self._cordY, poss[0], poss[1])
                            SelectHandler.Deselect()
                            TurnHandler.changeTurn()
                        elif (poss[0] == self._cordX + 1 or poss[0] == self._cordX - 1) and poss[
                            1] == self._cordY - 1 and TurnHandler.getPlayerTurn().getColor() == "C":
                            MoveH.Move(self._cordX, self._cordY, poss[0], poss[1])
                            SelectHandler.Deselect()
                            TurnHandler.changeTurn()
                        else:
                            raise exception.InvalidMove()
                    else:
                        if abs(poss[0] - self._cordX) == abs(poss[1] - self._cordY) and MoveH.NothingOnTheWay(
                                self._cordX, self._cordY, poss[0], poss[1]):
                            MoveH.Move(self._cordX, self._cordY, poss[0], poss[1])
                            SelectHandler.Deselect()
                            TurnHandler.changeTurn()
                        else:
                            raise exception.InvalidMove()
            elif SelectHandler.selected:
                poss = SelectHandler.getSelCords()
                tempx = self._cordX
                tempy = self._cordY
                if not MoveH.isQueen(poss[0], poss[1]):
                    if (poss[0] == self._cordX + 2 or poss[0] == self._cordX - 2) and poss[1] == self._cordY + 2:
                        print(str((poss[0] + self._cordX) // 2), str((poss[1] + self._cordY) // 2), "ssssssss")
                        MoveH.Kill((poss[0] + self._cordX) // 2, (poss[1] + self._cordY) // 2)
                        MoveH.Move(self._cordX, self._cordY, poss[0], poss[1])
                        if not MoveH.PossibleAttackX2(tempx, tempy):
                            TurnHandler.changeTurn()
                        SelectHandler.Deselect()
                    elif (poss[0] == self._cordX + 2 or poss[0] == self._cordX - 2) and poss[1] == self._cordY - 2:
                        MoveH.Kill((poss[0] + self._cordX) // 2, (poss[1] + self._cordY) // 2)
                        print(str((poss[0] + self._cordX) // 2), str((poss[1] + self._cordY) // 2), "ssssssss")
                        MoveH.Move(self._cordX, self._cordY, poss[0], poss[1])
                        # MoveH.PossibleAttackX2(poss[0], poss[1])
                        if not MoveH.PossibleAttackX2(tempx, tempy):
                            TurnHandler.changeTurn()
                        SelectHandler.Deselect()
                    else:
                        raise exception.AttackToDo(self.master)
                else:
                    if abs(poss[0] - self._cordX) == abs(poss[1] - self._cordY):
                        MoveH.Move(self._cordX, self._cordY, poss[0], poss[1])
                        MoveH.QueenKill(tempx, tempy, poss[0], poss[1])
                        SelectHandler.Deselect()
                        TurnHandler.changeTurn()
                    else:
                        raise exception.AttackToDo(self.master)
            else:
                raise exception.AttackToDo(self.master)

    def getPiece(self):
        return self._piece

    def setPiece(self, piece):
        self._piece = piece
        self.updateText()

    def updateText(self):
        if self._piece != None:
            self.TextToDisplay = self._piece.getTextToDisplay()
            self.config(textvariable=self.TextToDisplay)
        else:
            self.TextToDisplay = StringVar()
            self.TextToDisplay.set("")
            self.config(textvariable=self.TextToDisplay)

    def kill(self):
        del self._piece
        self._piece = None
        self.updateText()

    def promote(self):
        temp = Queen(self._cordX, self._cordY, self._piece.getColor())
        temp, self._piece = self._piece, temp
        del temp

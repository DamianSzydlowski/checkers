from tkinter import *

from Pieces.pawn import Pawn
from board.field import Field


class Board(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self._board = [[0 for i in range(8)]for i in range(8)]
        counter = 0
        for y in range(8):
            for x in range(8):
                if counter % 2 == 1:
                    if y < 3:
                        self._board[x][y] = Field(self, x, y, Pawn(x, y, "C"), bg = '#8D5A3C', width=6, height=3)
                    elif y > 4:
                        self._board[x][y] = Field(self, x, y, Pawn(x, y, "B"), bg = '#8D5A3C', width=6, height=3)
                    else:
                        self._board[x][y] = Field(self, x, y, bg = '#8D5A3C', width=6, height=3)
                else:
                    self._board[x][y] = Field(self, x, y, bg = '#E8DED8', width=6, height=3)
                self._board[x][y].grid(column = x, row = y)
                counter += 1
            counter += 1
        self.pack()

    def getBoard(self):
        return self._board




class BoardT1(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self._board = [[0 for i in range(8)]for i in range(8)]
        counter = 0
        for y in range(8):
            for x in range(8):
                if counter % 2 == 1:
                    if y < 3:
                        self._board[x][y] = Field(self, x, y, bg = '#8D5A3C', width=6, height=3)
                    elif y > 4:
                        self._board[x][y] = Field(self, x, y, bg = '#8D5A3C', width=6, height=3)
                    else:
                        self._board[x][y] = Field(self, x, y, bg = '#8D5A3C', width=6, height=3)
                else:
                    self._board[x][y] = Field(self, x, y, bg = '#E8DED8', width=6, height=3)
                self._board[x][y].grid(column = x, row = y)
                counter += 1
            counter += 1

        self._board[1][6].setPiece(Pawn(1, 6, "C"))
        self._board[1][6].grid(column=1, row=6)

        self._board[2][3].setPiece(Pawn(2, 3, "B"))
        self._board[2][3].grid(column=2, row=3)

        self.pack()

    def getBoard(self):
        return self._board

class BoardT2(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self._board = [[0 for i in range(8)]for i in range(8)]
        counter = 0
        for y in range(8):
            for x in range(8):
                if counter % 2 == 1:
                    if y < 3:
                        self._board[x][y] = Field(self, x, y, bg = '#8D5A3C', width=6, height=3)
                    elif y > 4:
                        self._board[x][y] = Field(self, x, y, bg = '#8D5A3C', width=6, height=3)
                    else:
                        self._board[x][y] = Field(self, x, y, bg = '#8D5A3C', width=6, height=3)
                else:
                    self._board[x][y] = Field(self, x, y, bg = '#E8DED8', width=6, height=3)
                self._board[x][y].grid(column = x, row = y)
                counter += 1
            counter += 1

        self._board[0][3].setPiece(Pawn(0, 3, "B"))
        self._board[0][3].grid(column=0, row=3)

        self._board[3][2].setPiece(Pawn(3, 2, "C"))
        self._board[3][2].grid(column=3, row=2)
        self._board[1][2].setPiece(Pawn(1, 2, "C"))
        self._board[1][2].grid(column=1, row=2)

        self.pack()

    def getBoard(self):
        return self._board
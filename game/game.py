from tkinter import *

from Pieces.piece import Piece
from board.board import Board, BoardT1, BoardT2
from handlers.moveHandler import MoveH
from handlers.turnHandler import TurnHandler
from interface.playerLabel import PlayerLabel
from interface.resetButton import ResetButton


class Game(Frame):
    def __init__(self, master, *args, **kwargs):
        Frame.__init__(self, master, *args, **kwargs)
        self.master = master

        self.setWindow()
        self.setTurnAndLabel()
        # self._board = Board(self.master)
        self._board = BoardT1(self.master)
        # self._board = BoardT2(self.master)
        self.setResetButton()
        self._moveH = MoveH(self._board)

    def start(self):
        self.master.mainloop()

    def reset(self):
        Piece.ResetNumber()
        self._board.destroy()
        # self._board = Board(self.master)
        self._board = BoardT1(self.master)
        # self._board = BoardT2(self.master)
        del self._turnH
        self._turnH = TurnHandler(self.PlayerTurnString)
        del self._moveH
        self._moveH = MoveH(self._board)



    def setWindow(self):
        self.pack()
        windowWidth = self.master.winfo_reqwidth() + 200
        windowHeight = self.master.winfo_reqheight() + 450
        positionRight = int(self.master.winfo_screenwidth() / 2 - windowWidth / 2)
        positionDown = int(self.master.winfo_screenheight() / 2 - windowHeight / 2)
        self.master.geometry("+{}+{}".format(positionRight, positionDown))
        self.master.title('Warcaby')
        self.master.resizable(0, 0)

    def setResetButton(self):
        self._resetButton = ResetButton(self.master, text="Reset Game", bg='#FF7F50', width=10, height=2,
                                        command=lambda: self.reset())
        self._resetButton.pack(side=BOTTOM, pady=20)

    def setTurnAndLabel(self):
        self.PlayerTurnString = StringVar()
        TurnHandler.setPlayerTurnStringVar(self.PlayerTurnString)
        self._turnH = TurnHandler(self.PlayerTurnString)
        self._playerLabel = PlayerLabel(self.master, textvariable=TurnHandler.PlayerTurnStringVar, font=4)
        self._playerLabel.pack(side=TOP, pady=10)

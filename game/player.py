class Player():

    def __init__(self, number, color):
        self._number = number
        self._color = color

    def __str__(self):
        return "Player " + str(self._number)

    def getColor(self):
        return self._color

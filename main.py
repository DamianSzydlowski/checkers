from tkinter import Tk

from game.game import Game

if __name__ == "__main__":
    root = Tk()
    Game(root).start()

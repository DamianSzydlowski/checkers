from tkinter import messagebox


class InvalidMove(Exception):
    def __str__(self):
        return "Invalid move"

    def display(self):
        messagebox.showerror("Error", str(self))


class NotYourPawn(Exception):
    def __init__(self, color):
        self.color = color

    def __str__(self):
        if self.color == "C":
            return "Black is not your color"
        else:
            return "White is not your color"

    def display(self):
        messagebox.showerror("Error", str(self))


class AttackToDo(Exception):
    def __str__(self):
        return "You need to attack first"

    def display(self):
        messagebox.showwarning("Warning", str(self))


class GameAlreadyEnded(Exception):
    def __str__(self):
        return "Game Already Ended"

    def display(self):
        messagebox.showerror("Error", str(self))


def TryBlock(x):
    def wraper(self):
        try:
            x(self)
        except (InvalidMove, NotYourPawn, AttackToDo, GameAlreadyEnded) as e:
            e.display()

    return wraper

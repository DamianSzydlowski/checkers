from tkinter import messagebox

from Pieces.pawn import Pawn
from Pieces.piece import Piece
from Pieces.quenn import Queen


class MoveH():
    possibleAttackB = False  # Możliwy atak czarnych
    possibleAttackW = False  # Możliwy atak białych
    toAttack = False  # Dowonly pionek mozliwy do zbicia
    board = None

    def __init__(self, board):
        MoveH.setBoard(board.getBoard())
        MoveH.possibleAttackB = False  #
        MoveH.possibleAttackW = False
        MoveH.toAttack = False
        MoveH.PossibleAttack()

    @staticmethod
    def Move(ToX, ToY, FromX, FromY):
        print(ToX, ToY)
        MoveH.board[ToX][ToY].setPiece(MoveH.board[FromX][FromY].getPiece())
        MoveH.board[ToX][ToY].getPiece().updatePoss(ToX, ToY)
        MoveH.board[FromX][FromY].setPiece(None)
        MoveH.board[ToX][ToY].getPiece().deselect()
        MoveH.board[FromX][FromY].updateText()
        MoveH.board[ToX][ToY].updateText()
        if MoveH.board[ToX][ToY].getPiece().getColor() == "C":
            if ToY == 7:
                MoveH.board[ToX][ToY].promote()
                MoveH.board[ToX][ToY].updateText()
        else:
            if ToY == 0:
                MoveH.board[ToX][ToY].promote()
                MoveH.board[ToX][ToY].updateText()
        # del
        print("trying to move", "W: ", Piece.countW, "B: ", Piece.countB)

    @staticmethod
    def setBoard(board):
        MoveH.board = board

    @staticmethod
    def Kill(x, y):
        # Pawn.NumberM(MoveH.board.getBoard()[x][y].getColor())
        MoveH.board[x][y].kill()
        print("Black: ", Piece.countB, "White: ", Piece.countW)
        if Piece.countW == 0:
            messagebox.showinfo("Koniec Gry", "Player 2 Win, press reset to start new Game")
        elif Piece.countB == 0:
            messagebox.showinfo("Koniec Gry", "Player 1 Win, press reset to start new Game")

    @staticmethod
    def QueenKill(ToX, ToY, FromX, FromY):
        diffX = ToX - FromX
        diffY = ToY - FromY
        if diffX > 0 and diffY > 0:
            for i in range(1, abs(diffX)):
                if MoveH.board[FromX + i][FromY + i].getPiece() != None:
                    MoveH.board[FromX + i][FromY + i].kill()
        elif diffX < 0 and diffY > 0:
            for i in range(1,abs(diffX)):
                if MoveH.board[FromX - i][FromY + i].getPiece() != None:
                    MoveH.board[FromX - i][FromY + i].kill()
        elif diffX > 0 and diffY < 0:
            for i in range(1, abs(diffX)):
                if MoveH.board[FromX + i][FromY - i].getPiece() != None:
                    MoveH.board[FromX + i][FromY - i].kill()
        else:
            for i in range(1, abs(diffX)):
                if MoveH.board[FromX - i][FromY - i].getPiece() != None:
                    MoveH.board[FromX - i][FromY - i].kill()

        print("Black: ", Piece.countB, "White: ", Piece.countW)
        if Piece.countW == 0:
            messagebox.showinfo("Koniec Gry", "Player 2 Win, press reset to start new Game")
        elif Piece.countB == 0:
            messagebox.showinfo("Koniec Gry", "Player 1 Win, press reset to start new Game")

    @staticmethod
    def NothingOnTheWay(ToX, ToY, FromX, FromY):
        diffX = ToX - FromX
        diffY = ToY - FromY
        if diffX > 0 and diffY > 0:
            for i in range(1, abs(diffX)):
                if MoveH.board[FromX + i][FromY + i].getPiece() != None:
                    return False
                else:
                    return True
        elif diffX < 0 and diffY > 0:
            for i in range(1, abs(diffX)):
                if MoveH.board[FromX - i][FromY + i].getPiece() != None:
                    return False
        elif diffX > 0 and diffY < 0:
            for i in range(1, abs(diffX)):
                if MoveH.board[FromX + i][FromY - i].getPiece() != None:
                    return False
                else:
                    return True
        else:
            for i in range(1, abs(diffX)):
                if MoveH.board[FromX - i][FromY - i].getPiece() != None:
                    return False
                else:
                    return True

    @staticmethod
    def PossibleAttack():
        MoveH.toAttack = False
        for x in range(8):
            for y in range(8):
                if type(MoveH.board[x][y].getPiece()) == Pawn:
                    # print("Im in", x, y)
                    if x + 2 < 8 and y + 2 < 8 and MoveH.board[x + 1][y + 1].getPiece() != None and MoveH.board[x + 1][
                        y + 1].getPiece().getColor() != MoveH.board[x][
                        y].getPiece().getColor() and MoveH.board[x + 2][y + 2].getPiece() == None:
                        print("to attack: ", x + 1, y + 1, MoveH.board[x + 1][y + 1].getPiece().getColor())
                        MoveH.toAttack = True
                        if "B" in MoveH.board[x][y].getPiece().getColor():
                            MoveH.possibleAttackW = True
                        else:
                            MoveH.possibleAttackB = True
                    elif x - 2 >= 0 and y + 2 < 8 and MoveH.board[x - 1][y + 1].getPiece() != None and \
                            MoveH.board[x - 1][
                                y + 1].getPiece().getColor() != MoveH.board[x][
                        y].getPiece().getColor() and MoveH.board[x - 2][y + 2].getPiece() == None:
                        print("to attack: ", x - 1, y + 1, MoveH.board[x - 1][y + 1].getPiece().getColor())
                        MoveH.toAttack = True
                        if "B" in MoveH.board[x][y].getPiece().getColor():
                            MoveH.possibleAttackW = True
                        else:
                            MoveH.possibleAttackB = True
                    elif x + 2 < 8 and y - 2 >= 0 and MoveH.board[x + 1][y - 1].getPiece() != None and \
                            MoveH.board[x + 1][
                                y - 1].getPiece().getColor() != MoveH.board[x][
                        y].getPiece().getColor() and MoveH.board[x + 2][y - 2].getPiece() == None:
                        print("to attack: ", x + 1, y - 1, MoveH.board[x + 1][y - 1].getPiece().getColor())
                        MoveH.toAttack = True
                        if "B" in MoveH.board[x][y].getPiece().getColor():
                            MoveH.possibleAttackW = True
                        else:
                            MoveH.possibleAttackB = True
                    elif x - 2 >= 0 and y - 2 >= 0 and MoveH.board[x - 1][y - 1].getPiece() != None and \
                            MoveH.board[x - 1][
                                y - 1].getPiece().getColor() != MoveH.board[x][
                        y].getPiece().getColor() and MoveH.board[x - 2][y - 2].getPiece() == None:
                        print("to attack: ", x - 1, y - 1, MoveH.board[x - 1][y - 1].getPiece().getColor())
                        MoveH.toAttack = True
                        if "B" in MoveH.board[x][y].getPiece().getColor():
                            MoveH.possibleAttackW = True
                        else:
                            MoveH.possibleAttackB = True
                    else:
                        if not MoveH.toAttack:
                            MoveH.possibleAttackB = False
                            MoveH.possibleAttackW = False
                elif type(MoveH.board[x][y].getPiece()) == Queen:
                    i = 1
                    while x + i + 1 < 8 and y + i + 1 < 8:
                        if MoveH.board[x + i][y + i].getPiece() != None and MoveH.board[x + i][
                            y + i].getPiece().getColor() != MoveH.board[x][y].getPiece().getColor():
                            if MoveH.board[x + i + 1][y + i + 1].getPiece() == None:
                                print("to attack: ", x + i, y + i, MoveH.board[x + i][y + i].getPiece().getColor())
                                MoveH.toAttack = True
                                if "B" in MoveH.board[x][y].getPiece().getColor():
                                    MoveH.possibleAttackW = True
                                else:
                                    MoveH.possibleAttackB = True
                            else:
                                break
                        i += 1
                    i = 1
                    while x + i + 1 < 8 and y - i - 1 >= 0:
                        if MoveH.board[x + i][y - i].getPiece() != None and MoveH.board[x + i][
                            y - i].getPiece().getColor() != MoveH.board[x][y].getPiece().getColor():
                            if MoveH.board[x + i + 1][y - i - 1].getPiece() == None:
                                print("to attack: ", x + i, y - i, MoveH.board[x + i][y - i].getPiece().getColor())
                                MoveH.toAttack = True
                                if "B" in MoveH.board[x][y].getPiece().getColor():
                                    MoveH.possibleAttackW = True
                                else:
                                    MoveH.possibleAttackB = True
                            else:
                                break
                        i += 1
                    i = 1
                    while x - i - 1 >= 0 and y + i + 1 < 8:
                        if MoveH.board[x - i][y + i].getPiece() != None and MoveH.board[x - i][
                            y + i].getPiece().getColor() != MoveH.board[x][y].getPiece().getColor():
                            if MoveH.board[x - i - 1][y + i + 1].getPiece() == None:
                                print("to attack: ", x - i, y + i, MoveH.board[x - i][y + i].getPiece().getColor())
                                MoveH.toAttack = True
                                if "B" in MoveH.board[x][y].getPiece().getColor():
                                    MoveH.possibleAttackW = True
                                else:
                                    MoveH.possibleAttackB = True
                            else:
                                break
                        i += 1
                    i = 1
                    while x - i - 1 >= 0 and y - i - 1 >= 0:
                        if MoveH.board[x - i][y - i].getPiece() != None and MoveH.board[x - i][
                            y - i].getPiece().getColor() != MoveH.board[x][y].getPiece().getColor():
                            if MoveH.board[x - i - 1][y - i - 1].getPiece() == None:
                                print("to attack: ", x - i, y - i, MoveH.board[x - i][y - i].getPiece().getColor())
                                MoveH.toAttack = True
                                if "B" in MoveH.board[x][y].getPiece().getColor():
                                    MoveH.possibleAttackW = True
                                else:
                                    MoveH.possibleAttackB = True
                            else:
                                break
                        i += 1
                else:
                    pass

        if MoveH.toAttack:
            print("smth to attack")

    @staticmethod
    def PossibleAttackX2(x, y):
        if type(MoveH.board[x][y].getPiece()) == Pawn:
            if x + 2 < 8 and y + 2 < 8 and type(
                    MoveH.board[x + 1][y + 1].getPiece()) == Pawn and MoveH.board[x + 1][y + 1].getPiece().getColor() != \
                    MoveH.board[x][
                        y].getPiece().getColor() and MoveH.board[x + 2][y + 2].getPiece() == None:
                print("to attack: ", x + 1, y + 1, MoveH.board[x + 1][y + 1].getPiece().getColor())
                MoveH.toAttack = True
                if "B" in MoveH.board[x][y].getPiece().getColor():
                    MoveH.possibleAttackW = True
                else:
                    MoveH.possibleAttackB = True
                return True
            elif x - 2 >= 0 and y + 2 < 8 and type(MoveH.board[x - 1][y + 1].getPiece()) == Pawn and \
                    MoveH.board[x - 1][
                        y + 1].getPiece().getColor() != MoveH.board[x][
                y].getPiece().getColor() and MoveH.board[x - 2][y + 2].getPiece() == None:
                print("to attack: ", x - 1, y + 1, MoveH.board[x - 1][y + 1].getPiece().getColor())
                MoveH.toAttack = True
                if "B" in MoveH.board[x][y].getPiece().getColor():
                    MoveH.possibleAttackW = True
                else:
                    MoveH.possibleAttackB = True
                return True
            elif x + 2 < 8 and y - 2 >= 0 and type(MoveH.board[x + 1][y - 1].getPiece()) == Pawn and \
                    MoveH.board[x + 1][
                        y - 1].getPiece().getColor() != MoveH.board[x][
                y].getPiece().getColor() and MoveH.board[x + 2][y - 2].getPiece() == None:
                print("to attack: ", x + 1, y - 1, MoveH.board[x + 1][y - 1].getPiece().getColor())
                MoveH.toAttack = True
                if "B" in MoveH.board[x][y].getPiece().getColor():
                    MoveH.possibleAttackW = True
                else:
                    MoveH.possibleAttackB = True
                return True
            elif x - 2 >= 0 and y - 2 >= 0 and type(MoveH.board[x - 1][y - 1].getPiece()) == Pawn and \
                    MoveH.board[x - 1][
                        y - 1].getPiece().getColor() != MoveH.board[x][
                y].getPiece().getColor() and MoveH.board[x - 2][y - 2].getPiece() == None:
                print("to attack: ", x - 1, y - 1, MoveH.board[x - 1][y - 1].getPiece().getColor())
                MoveH.toAttack = True
                if "B" in MoveH.board[x][y].getPiece().getColor():
                    MoveH.possibleAttackW = True
                else:
                    MoveH.possibleAttackB = True
                return True
            else:
                if not MoveH.toAttack:
                    MoveH.possibleAttackB = False
                    MoveH.possibleAttackW = False
                return False

    @staticmethod
    def isQueen(x, y):
        if type(MoveH.board[x][y].getPiece()) == Queen:
            return True
        else:
            return False
